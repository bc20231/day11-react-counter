import { useEffect, useState } from "react";
import CounterSizeGenerator from "./CounterSizeGenerator";
import CounterGroup from "./CounterGroup";
import CounterGroupSum from "./CounterGroupSum";
import './MultipleCounter.css';

const MultipleCounter = () => {
    const [size, setSize] = useState(0);
    const [sum, setSum] = useState(0);
    const [reset, setReset] = useState(false);

    useEffect(() => {
        setReset(true);
        setTimeout(() => setReset(false), 100);
    }, [size]);

    return (
        <div>
            <CounterSizeGenerator onSizeChange={setSize} setSum={setSum} setReset={setReset}/>
            <CounterGroupSum sum={sum} />
            <CounterGroup setSum={setSum} size={size} reset={reset}/>
        </div>
    );
}

export default MultipleCounter;