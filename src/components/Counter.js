import { useEffect, useState } from "react";

const Counter = ({setSum, reset}) => {
    const [count, setCount] = useState(0);

    useEffect(() => {
        if (reset) {
            setCount(0);
        }
    }, [reset]);

    const handleAdd = () => {
        setCount(count + 1);
        setSum(prevSum => prevSum + 1);
    };

    const handleSubtract = () => {
        setCount(count - 1);
        setSum(prevSum => prevSum - 1);
    };

    return (
        <div>
            <button onClick={handleAdd}>+</button>
            <span>{count}</span>
            <button onClick={handleSubtract}>-</button>
        </div>
    );
}

export default Counter;