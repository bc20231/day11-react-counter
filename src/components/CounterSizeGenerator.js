const CounterSizeGenerator = ({onSizeChange, setSum, setReset}) => {
    return (
        <div className="size">
            <label>Size: </label>
            <input type="number" onChange={(e) => {
                setSum(0);
                setReset(true);
                onSizeChange(Number(e.target.value));
            }} />
        </div>
    );
}

export default CounterSizeGenerator;