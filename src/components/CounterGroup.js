import Counter from "./Counter";

const CounterGroup = ({size, setSum, reset}) => {
    const counters = [];
    for (let i = 0; i < size; i++) {
        counters.push(<Counter key={i} setSum={setSum} reset={reset}/>);
    }

    return (
        <div>{counters}</div>
    );
}

export default CounterGroup;